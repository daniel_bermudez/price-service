 # Spring Boot Service Price Project
 ## **Service description**


This service is responsible for managing the price of a product for an e-commerce company.

Specifically, given a product identifier, an identifier brand and a date on which the price of the product is requested, the service is in charge of consulting the corresponding price in the database.


In the database we have a table **"PRICES"** that contains the following information:

| Field name | description |
| ------ | ------ |
|   **BRAND_ID**     |   Identifies the chain of the group within the company.     |
|     **START_DATE, END_DATE**   |  Date range in which a price is applied to a product.      |
|    **PRICE_LIST**    |  Applicable price rate identifier.      |
|   **PRODUCT_ID**     |   Product identifier.     |
|     **PRIORITY**   |   If two rates coincide in a range of dates, the one with the highest priority (greatest numerical value) is applied.     |
|   **PRICE**     |     Final sale price.   |
|   **CURR**     |   ISO of the currency.     |


Therefore, the application provides a REST endpoint that allows you to check the price of a product.

- **Input parameters:** PRODUCT_ID, BRAND_ID and Date.

- **Response:** Response object with the fields (PRODUCT_ID, BRAND_ID, PRICE_LIST, START_DATE, END_DATE and PRICE).


## Requirements

For building and running the application you need:

- [JDK 17](https://www.oracle.com/java/technologies/downloads/#java17)
- [Maven 3](https://maven.apache.org/download.cgi)

## Running the application locally
There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.ecommerce.priceservice.PriceServiceApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

You can build the project and run the tests by running ```mvn clean package```

## Use case
Once you have the application up, you can test the endpoint at the following URL:

[http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

In the previous link you can access Swagger and view the endpoint information to obtain the price of a product.

To make a valid request in the browser click [here](http://localhost:8080/api/v1/prices/product/35455/brand/1/date/2020-06-14T01%3A30%3A00).

Curl example:

```shell
curl -X GET http://localhost:8080/api/v1/prices/product/35455/brand/1/date/2020-06-14T01%3A30%3A00
```
