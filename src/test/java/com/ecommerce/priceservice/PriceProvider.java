package com.ecommerce.priceservice;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

import com.ecommerce.priceservice.domain.model.Price;

public class PriceProvider {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");

	public static Stream<Arguments> providePricesForTest() {
		return Stream.of(Arguments.of(35455, 1, LocalDateTime.parse("2020-06-14 10.00.00", formatter), 35.50),
				Arguments.of(35455, 1, LocalDateTime.parse("2020-06-14 16.00.00", formatter), 25.45),
				Arguments.of(35455, 1, LocalDateTime.parse("2020-06-14 21.00.00", formatter), 35.50),
				Arguments.of(35455, 1, LocalDateTime.parse("2020-06-15 10.00.00", formatter), 30.50),
				Arguments.of(35455, 1, LocalDateTime.parse("2020-06-16 21.00.00", formatter), 38.95));
	}

	public static Stream<Arguments> providePricesForTestNotFoundException() {
		return Stream.of(Arguments.of(35455, 1, LocalDateTime.parse("1999-06-14 10.00.00", formatter)),
				Arguments.of(35455, 1, LocalDateTime.parse("2025-06-14 16.00.00", formatter)),
				Arguments.of(35455, 1, LocalDateTime.parse("2010-06-14 21.00.00", formatter)));
	}

	public static List<Price> providePricesFromDataBase() {
		return Arrays.asList(
				new Price(1, LocalDateTime.parse("2020-06-14 00.00.00", formatter),
						LocalDateTime.parse("2020-12-31 23.59.59", formatter), 1, 35455, 0, 35.50, "EUR"),
				new Price(1, LocalDateTime.parse("2020-06-14 15.00.00", formatter),
						LocalDateTime.parse("2020-06-14 18.30.00", formatter), 2, 35455, 1, 25.45, "EUR"),
				new Price(1, LocalDateTime.parse("2020-06-15 00.00.00", formatter),
						LocalDateTime.parse("2020-06-15 11.00.00", formatter), 3, 35455, 1, 30.50, "EUR"),
				new Price(1, LocalDateTime.parse("2020-06-15 16.00.00", formatter),
						LocalDateTime.parse("2020-12-31 23.59.59", formatter), 4, 35455, 1, 38.95, "EUR"));
	}

}
