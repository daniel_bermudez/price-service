package com.ecommerce.priceservice.infrastructure.rest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class PriceRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@ParameterizedTest(name = "Test {index}: ProductId: {0}, BrandId: {1}, date: {2} = {3} final price")
	@MethodSource("com.ecommerce.priceservice.PriceProvider#providePricesForTest")
	void givenProductIdBrandIdAndDateTime_whenRestGetRequest_ThenReturnCorrectPrice(long productId, int brandId,
			LocalDateTime date, double priceExpected) throws Exception {

		mvc.perform(get("/api/v1/prices/product/{productId}/brand/{brandId}/date/{date}", productId, brandId, date)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("price", equalTo(priceExpected)));

	}

}
