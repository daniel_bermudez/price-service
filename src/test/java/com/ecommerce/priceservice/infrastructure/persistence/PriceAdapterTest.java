package com.ecommerce.priceservice.infrastructure.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ecommerce.priceservice.infrastructure.persistence.adapters.PriceAdapter;
import com.ecommerce.priceservice.infrastructure.persistence.repositories.PriceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ecommerce.priceservice.domain.model.Price;
import com.ecommerce.priceservice.infrastructure.persistence.entities.PriceEntity;

class PriceAdapterTest {

	private PriceRepository priceRepository;

	private PriceAdapter priceAdapter;

	private static DateTimeFormatter formatter;

	@BeforeEach
	public void setUp() {
		priceRepository = mock(PriceRepository.class);
		priceAdapter = new PriceAdapter(priceRepository);
		formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");
	}

	@Test
	void givenProductIdAndBrandId_whenFindPriceInRepository_ThenReturnSameSizePrices() {
		// Given
		int brandId = 1;
		long productId = 35455;
		List<PriceEntity> pricesEntity = Arrays
				.asList(new PriceEntity(1, LocalDateTime.parse("2020-06-14 00.00.00", formatter),
						LocalDateTime.parse("2020-12-31 23.59.59", formatter), 1, 35455, 0, 35.50, "EUR"));

		when(priceRepository.findByBrandIdAndProductId(brandId, productId)).thenReturn(pricesEntity);

		// When
		List<Price> prices = priceAdapter.findByBrandIdAndProductId(brandId, productId);

		// Then
		assertThat(prices.size()).isEqualTo(pricesEntity.size());
		verify(priceRepository).findByBrandIdAndProductId(brandId, productId);

	}

	@Test
	void givenProductIdAndBrandId_whenFindPriceInRepository_ThenReturnEmptyList() {
		// Given
		int brandId = 2;
		long productId = 1111;
		List<PriceEntity> pricesEntity = new ArrayList<>();

		when(priceRepository.findByBrandIdAndProductId(brandId, productId)).thenReturn(pricesEntity);

		// When
		List<Price> prices = priceAdapter.findByBrandIdAndProductId(brandId, productId);

		// Then

		assertThat(prices).isEmpty();
		verify(priceRepository).findByBrandIdAndProductId(brandId, productId);

	}

}
