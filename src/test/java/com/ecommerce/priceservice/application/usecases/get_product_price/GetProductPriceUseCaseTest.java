package com.ecommerce.priceservice.application.usecases.get_product_price;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;

import com.ecommerce.priceservice.application.usecases.get_product_price.GetProductPriceUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.ecommerce.priceservice.PriceProvider;
import com.ecommerce.priceservice.domain.exception.PriceNotFoundException;
import com.ecommerce.priceservice.domain.port.PricePort;
import com.ecommerce.priceservice.domain.model.Price;

class GetProductPriceUseCaseTest {

	private PricePort pricePort;

	private GetProductPriceUseCase getProductPriceUseCase;

	private List<Price> prices;

	@BeforeEach
	public void setUp() {
		pricePort = mock(PricePort.class);
		getProductPriceUseCase = new GetProductPriceUseCase(pricePort);
		prices = PriceProvider.providePricesFromDataBase();

	}

	@ParameterizedTest(name = "Test {index}: ProductId: {0}, BrandId: {1}, date: {2} = {3} final price")
	@MethodSource("com.ecommerce.priceservice.PriceProvider#providePricesForTest")
	void givenProductIdBrandIdAndDateTime_whenFindPrice_ThenReturnPrice(long productId, int brandId, LocalDateTime date,
			double priceExpected) {
		// Given
		when(pricePort.findByBrandIdAndProductId(brandId, productId)).thenReturn(prices);

		// When
		Price price = getProductPriceUseCase.getPriceProductByProductBrandAndDate(productId, brandId, date);

		// Then
		assertThat(priceExpected).isEqualTo(price.getPrice());

	}

	@ParameterizedTest(name = "Test {index}: ProductId: {0}, BrandId: {1}, date: {2} = PriceNotFoundException")
	@MethodSource("com.ecommerce.priceservice.PriceProvider#providePricesForTestNotFoundException")
	void givenProductIdBrandIdAndDateTime_whenFindPrice_ThenThrowsPriceNotFoundExceptionException(long productId,
			int brandId, LocalDateTime date) {
		// Given
		when(pricePort.findByBrandIdAndProductId(brandId, productId)).thenReturn(prices);

		// When
		Throwable thrown = catchThrowable(() -> {
			getProductPriceUseCase.getPriceProductByProductBrandAndDate(productId, brandId, date);
		});

		// Then
		assertThat(thrown).isInstanceOf(PriceNotFoundException.class);

	}

}
