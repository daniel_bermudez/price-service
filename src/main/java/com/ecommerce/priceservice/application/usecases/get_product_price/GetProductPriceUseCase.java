package com.ecommerce.priceservice.application.usecases.get_product_price;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import com.ecommerce.priceservice.domain.exception.PriceNotFoundException;
import com.ecommerce.priceservice.domain.port.PricePort;
import com.ecommerce.priceservice.domain.model.Price;

@Service
@RequiredArgsConstructor
@Slf4j
public class GetProductPriceUseCase {

    private final PricePort pricePort;

    public Price getPriceProductByProductBrandAndDate(long productId, int brandId, LocalDateTime date) {
        log.info("Init find price with brandId: {}, and productId= {}, in date= {}", brandId, productId, date);
        List<Price> prices = pricePort.findByBrandIdAndProductId(brandId, productId);
        return prices.stream().filter(priceActual -> priceActual.isPriceInDate(date))
                .max(Comparator.comparing(Price::getPriority))
                .orElseThrow(() -> new PriceNotFoundException(
                        "Price was not found for parameters {productId=%s,brandId=%s,date=%s}".formatted(productId,
                                brandId, date)));
    }

}
