package com.ecommerce.priceservice.infrastructure.persistence.adapters;

import com.ecommerce.priceservice.domain.model.Price;
import com.ecommerce.priceservice.domain.port.PricePort;
import com.ecommerce.priceservice.infrastructure.persistence.repositories.PriceRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@AllArgsConstructor
@Slf4j
@Repository
public class PriceAdapter implements PricePort {

    private final PriceRepository priceRepository;

    @Override
    public List<Price> findByBrandIdAndProductId(int brandId, long productId) {
        log.info("Init find price with brandId: {}, and productId= {}.", brandId, productId);
        ModelMapper mapper = new ModelMapper();
        return priceRepository.findByBrandIdAndProductId(brandId, productId).stream()
                .map(priceActual -> mapper.map(priceActual, Price.class)).toList();
    }

}
