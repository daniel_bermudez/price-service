package com.ecommerce.priceservice.infrastructure.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.priceservice.infrastructure.persistence.entities.PriceEntity;

public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

	List<PriceEntity> findByBrandIdAndProductId(int brandId, long productId);

}
