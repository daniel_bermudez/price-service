package com.ecommerce.priceservice.infrastructure.persistence.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@Entity
@Table(name = "PRICES")
public class PriceEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "BRAND_ID")
	private int brandId;

	@Column(name = "START_DATE")
	private LocalDateTime startDate;

	@Column(name = "END_DATE")
	private LocalDateTime endDate;

	@Column(name = "PRICE_LIST")
	private int priceList;

	@Column(name = "PRODUCT_ID")
	private long productId;

	@Column(name = "PRIORITY")
	private int priority;

	@Column(name = "PRICE")
	private double price;

	@Column(name = "CURR")
	private String curr;

	public PriceEntity(int brandId, LocalDateTime startDate, LocalDateTime endDate, int priceList, long productId,
			int priority, double price, String curr) {
		this.brandId = brandId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.priceList = priceList;
		this.productId = productId;
		this.priority = priority;
		this.price = price;
		this.curr = curr;
	}

}
