package com.ecommerce.priceservice.infrastructure.rest.handlerexception;

import com.ecommerce.priceservice.domain.exception.PriceNotFoundException;
import com.ecommerce.priceservice.infrastructure.rest.dto.ErrorDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class RestControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(PriceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ErrorDTO priceNotFoundExceptionHandler(PriceNotFoundException ex) {
        return buildError(ex);
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ErrorDTO globalExceptionHandler(Exception ex) {
        log.error("Error message", ex);
        return buildError(ex);
    }

    private ErrorDTO buildError(Exception ex) {
        return ErrorDTO.builder()
                .description(ex.getMessage())
                .build();
    }
}
