package com.ecommerce.priceservice.infrastructure.rest;

import com.ecommerce.priceservice.infrastructure.rest.dto.ErrorDTO;
import com.ecommerce.priceservice.infrastructure.rest.dto.PriceDTO;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;

@RequestMapping("/api/v1/prices")
@OpenAPIDefinition(info = @Info(title = "Product prices API", version = "1.0", description = "Product prices information"))
public interface PriceApi {

    @Operation(summary = "Get a product price by productId, brandId and a date")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the price product", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PriceDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Product price not found", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorDTO.class))}),
            @ApiResponse(responseCode = "500", description = "Indicates that an unexpected error occurred while getting the product price", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorDTO.class))})})
    @GetMapping("/product/{productId}/brand/{brandId}/date/{date}")
    ResponseEntity<PriceDTO> getPriceProduct(
            @Parameter(description = "Product identifier", example = "35455") @PathVariable long productId,
            @Parameter(description = "Brand identifier", example = "1") @PathVariable int brandId,
            @Parameter(description = "Product price request date", example = "2020-06-14T01:30:00") @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date);

}
