package com.ecommerce.priceservice.infrastructure.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Contains price data")
public class PriceDTO {
    @Schema(description = "Product identifier", example = "35455")
    private long productId;
    @Schema(description = "Brand identifier", example = "1")
    private int brandId;
    @Schema(description = "Applicable price rate identifier", example = "1")
    private int priceList;
    @Schema(description = "Date range in which a price is applied to a product", example = "2020-06-14T00:00:00")
    private LocalDateTime startDate;
    @Schema(description = "Date range in which a price is applied to a product", example = "2020-12-31T23:59:59")
    private LocalDateTime endDate;
    @Schema(description = "Final sale price", example = "35.5")
    private double price;


}
