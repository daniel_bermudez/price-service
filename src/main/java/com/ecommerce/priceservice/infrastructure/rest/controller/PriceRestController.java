package com.ecommerce.priceservice.infrastructure.rest.controller;

import com.ecommerce.priceservice.application.usecases.get_product_price.GetProductPriceUseCase;
import com.ecommerce.priceservice.infrastructure.rest.PriceApi;
import com.ecommerce.priceservice.infrastructure.rest.dto.PriceDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
public class PriceRestController implements PriceApi {

    private final GetProductPriceUseCase getProductPriceUseCase;

    @Override
    public ResponseEntity<PriceDTO> getPriceProduct(long productId, int brandId, LocalDateTime date) {
        ModelMapper mapper = new ModelMapper();
        return ResponseEntity.ok(mapper.map(getProductPriceUseCase.getPriceProductByProductBrandAndDate(productId, brandId, date),
                PriceDTO.class));
    }

}
