package com.ecommerce.priceservice.infrastructure.rest.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Schema(description = "Contains the error message produced")
public class ErrorDTO {
    @Schema(description = "Error message description", example = "An error has occurred in the request")
    private String description;

}
