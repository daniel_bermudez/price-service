package com.ecommerce.priceservice.domain.port;

import java.util.List;

import com.ecommerce.priceservice.domain.model.Price;

public interface PricePort {

    List<Price> findByBrandIdAndProductId(int brandId, long productId);

}
