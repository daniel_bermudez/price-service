package com.ecommerce.priceservice.domain.exception;

public final class PriceNotFoundException extends RuntimeException {

	public PriceNotFoundException(final String message) {
		super(message);
	}

}