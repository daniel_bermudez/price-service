package com.ecommerce.priceservice.domain.model;

import java.time.LocalDateTime;

public class Price {

	private int brandId;

	private LocalDateTime startDate;

	private LocalDateTime endDate;

	private int priceList;

	private long productId;

	private int priority;

	private double price;

	private String curr;

	public Price() {

	}

	public Price(int brandId, LocalDateTime startDate, LocalDateTime endDate, int priceList, long productId,
			int priority, double price, String curr) {
		this.brandId = brandId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.priceList = priceList;
		this.productId = productId;
		this.priority = priority;
		this.price = price;
		this.curr = curr;
	}

	public int getBrandId() {
		return brandId;
	}

	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public int getPriceList() {
		return priceList;
	}

	public void setPriceList(int priceList) {
		this.priceList = priceList;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCurr() {
		return curr;
	}

	public void setCurr(String curr) {
		this.curr = curr;
	}

	public boolean isPriceInDate(LocalDateTime date) {
		return (this.startDate.isEqual(date) || this.endDate.isEqual(date)
				|| (this.startDate.isBefore(date) && this.endDate.isAfter(date)));
	}

}
